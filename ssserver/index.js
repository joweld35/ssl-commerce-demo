
const { MongoClient, ServerApiVersion, ObjectId } = require('mongodb');
const express = require('express')
require('dotenv').config()
const axios = require('axios');


const cors = require('cors')
const app = express()
const port =process.env.PORT||3000

app.use(cors())
app.use(express.json())
app.use(express.urlencoded({ extended: true }))
const qs = require("qs")


const uri = `mongodb+srv://${process.env.DB_USER}:${process.env.DB_PASS}@cluster0.4mwwnz0.mongodb.net/?retryWrites=true&w=majority&appName=Cluster0`;

const client = new MongoClient(uri, {
  serverApi: {
    version: ServerApiVersion.v1,
    strict: true,
    deprecationErrors: true,
  }
});

async function run() {
  try {
    const payments =client.db('ssl').collection('payment')


    app.post('/create-payment',async(req,res)=>{
      const payment = req.body
      const trxId=new ObjectId().toString()
      const initiateData={
        store_id:"conte666bf61b86c08",
        store_passwd:"conte666bf61b86c08@ssl",
        total_amount:payment.price,
        currency:"USD",
        tran_id:trxId,
        success_url:"http://localhost:3000/success-payment",
        fail_url:"http://localhost:5173/fail",
        cancel_url:"http://localhost:5173/cancel",
        cus_name:"Customer Name&",
        product_name:"laptop",
        product_category:"Electronics",
        product_profile:'ok',
        cus_email:"cust@yahoo.com&",
        cus_add1:"Dhaka&",
        cus_add2:"Dhaka&",
        cus_city:"Dhaka&",
        cus_state:"Dhaka&",
        cus_postcode:"1000&",
        cus_country:"Bangladesh&",
        cus_phone:"01711111111&",
        cus_fax:"01711111111&",
        shipping_method:"NO",
              
        multi_card_name:"mastercard,visacard,amexcard&",
        value_a:"ref001_A&",
        value_b:"ref002_B&",
        value_c:"ref003_C&",
        value_d:"ref004_D",

      }

      const response = await axios.post(
        'https://sandbox.sslcommerz.com/gwprocess/v4/api.php',
        qs.stringify(initiateData)
        ,
        {  
            headers: {"Content-Type": "application/x-www-form-urlencoded"}
        }
      )
      if(response.data){
        const saveData ={
          cus_name : "DUMMY",
          paymentId:trxId,
          amount:payment.price,
          status:"pending"
        }
        const save =  await payments.insertOne(saveData)
        if(save){
          res.send(response.data)
        }
      }
    })
    


  app.post('/success-payment',async(req,res)=>{
    const successData = req.body

    if(successData.status!=="VALID"){
       throw new Error("Unauthorized Payment")
    }

    const query = {
      paymentId : successData.tran_id
    }

    const update = {
      $set:{
        status:"Success"
      }
    }

    const updated =await payments.updateOne(query,update)
    if(updated.modifiedCount>0){
      res.redirect('http://localhost:5173/success')
    }



    


  })











    console.log("Pinged your deployment. You successfully connected to MongoDB!");
  } finally {

  }
}
run().catch(console.dir);















app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})